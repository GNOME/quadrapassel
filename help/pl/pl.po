# Polish translation for quadrapassel help.
# Copyright © 2017-2023 the quadrapassel authors.
# This file is distributed under the same license as the quadrapassel help.
# Piotr Drąg <piotrdrag@gmail.com>, 2017-2023.
# Aviary.pl <community-poland@mozilla.org>, 2017-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: quadrapassel-help\n"
"POT-Creation-Date: 2024-03-05 14:18+0000\n"
"PO-Revision-Date: 2023-09-16 15:49+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Piotr Drąg <piotrdrag@gmail.com>, 2017-2023\n"
"Aviary.pl <community-poland@mozilla.org>, 2017-2023"

#. (itstool) path: credit/name
#: C/bug-filing.page:11 C/develop.page:12 C/documentation.page:10
#: C/playgame.page:15 C/preferences.page:15 C/score.page:15 C/translate.page:10
msgid "Tiffany Antopolski"
msgstr "Tiffany Antopolski"

#. (itstool) path: credit/years
#: C/bug-filing.page:13 C/develop.page:14 C/documentation.page:12
#: C/index.page:14 C/playgame.page:12 C/preferences.page:12 C/score.page:12
#: C/translate.page:12
msgid "2012"
msgstr "2012"

#. (itstool) path: credit/name
#: C/bug-filing.page:16 C/develop.page:17 C/documentation.page:15
#: C/index.page:12 C/playgame.page:10 C/preferences.page:10 C/score.page:10
#: C/translate.page:15
msgid "Elena Petrevska"
msgstr "Elena Petrevska"

#. (itstool) path: license/p
#: C/bug-filing.page:20 C/develop.page:21 C/documentation.page:19
#: C/index.page:17 C/preferences.page:19 C/score.page:19
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Share Alike 3.0"

#. (itstool) path: info/desc
#: C/bug-filing.page:22
msgid "Found a problem? Feel free to file a bug."
msgstr "Wystąpił problem? Prosimy go zgłosić."

#. (itstool) path: page/title
#: C/bug-filing.page:25
msgid "Report a Problem"
msgstr "Zgłaszanie problemów"

#. (itstool) path: page/p
#: C/bug-filing.page:26
msgid ""
"If you notice a problem you can file a <em>bug report</em>. To file a bug, "
"go to <link href=\"https://gitlab.gnome.org/GNOME/quadrapassel/issues/\"/>."
msgstr ""
"Jeśli wystąpił jakiś błąd, to można go zgłosić na stronie <link "
"href=\"https://gitlab.gnome.org/GNOME/quadrapassel/issues/\"/> (w języku "
"angielskim)."

#. (itstool) path: page/p
#: C/bug-filing.page:29
msgid ""
"This is a bug tracking system where users and developers can file details "
"about bugs, crashes and request enhancements."
msgstr ""
"To system śledzenia błędów, w którym użytkownicy i programiści mogą zgłaszać "
"informacje o błędach, awariach i prośby o ulepszenia."

#. (itstool) path: page/p
#: C/bug-filing.page:32
msgid ""
"To participate you need an account which will give you the ability to gain "
"access, file bugs, and make comments. Also, you need to register so you can "
"receive updates by e-mail about the status of your bug. If you don't already "
"have an account, just click on the <gui>Sign in / Register</gui> link to "
"create one."
msgstr ""
"Do wzięcia udziału potrzebne jest konto, dające dostęp, możliwość zgłaszania "
"błędów i pisania komentarzy. Rejestracja umożliwia także otrzymywanie "
"powiadomień pocztą e-mail o stanie błędu. Jeśli nie masz jeszcze konta, to "
"kliknij odnośnik <gui>Sign in / Register</gui> (Zaloguj się/zarejestruj)."

#. (itstool) path: page/p
#: C/bug-filing.page:36
msgid ""
"Once you have an account, log in, and click on <gui>New issue</gui>. Before "
"reporting a bug, please read the <link href=\"https://wiki.gnome.org/"
"GettingInTouch/BugReportingGuidelines\">bug writing guidelines</link>, and "
"please <link href=\"https://gitlab.gnome.org/GNOME/quadrapassel/"
"issues\">browse</link> for the bug to see if it already exists."
msgstr ""
"Po utworzeniu konta zaloguj się i kliknij <gui>New issue</gui> (Nowe "
"zgłoszenie). Przed zgłoszeniem błędu prosimy przeczytać <link href=\"https://"
"wiki.gnome.org/GettingInTouch/BugReportingGuidelines\">wytyczne opisywania "
"błędów</link> i <link href=\"https://gitlab.gnome.org/GNOME/quadrapassel/"
"issues\">poszukać</link>, czy błąd został już zgłoszony."

#. (itstool) path: page/p
#: C/bug-filing.page:41
msgid ""
"If you are requesting a new feature, choose <gui>1. Feature</gui> in the "
"<gui>Labels</gui> menu. Fill in the Title and Description sections and click "
"<gui>Submit Issue</gui>."
msgstr ""
"W przypadku prośby o nową funkcję wybierz <gui>1. Feature</gui> (1. Funkcja) "
"w menu <gui>Labels</gui> (Etykiety). Wypełnij sekcje „Title” (Tytuł) "
"i „Description” (Opis) w języku angielskim i kliknij przycisk <gui>Submit "
"Issue</gui> (Wyślij zgłoszenie)."

#. (itstool) path: page/p
#: C/bug-filing.page:45
msgid ""
"Your report will be given an ID number, and its status will be updated as it "
"is being dealt with."
msgstr ""
"Zgłoszenie otrzyma numer identyfikujący, a jego stan będzie zmieniany razem "
"z postępem jego rozwiązywania."

#. (itstool) path: section/p
#: C/bug-filing.page:50 C/develop.page:35 C/documentation.page:35
#: C/translate.page:47
msgid "<link xref=\"index#get-involved\"><_:media-1/>Get Involved</link>"
msgstr "<link xref=\"index#get-involved\"><_:media-1/> Dołącz do nas</link>"

#. (itstool) path: section/p
#: C/bug-filing.page:51 C/develop.page:36 C/documentation.page:36
#: C/playgame.page:48 C/preferences.page:71 C/score.page:38 C/translate.page:48
msgid "<link xref=\"index\"><_:media-1/>HOME</link>"
msgstr "<link xref=\"index\"><_:media-1/> Strona główna</link>"

#. (itstool) path: info/desc
#: C/develop.page:23
msgid "Have a plan for developing the game? We are always open to new ideas."
msgstr "Masz pomysł na rozwój gry? Jesteśmy otwarci na nowe pomysły."

#. (itstool) path: page/title
#: C/develop.page:26
msgid "Help develop"
msgstr "Pomoc w programowaniu"

#. (itstool) path: page/p
#: C/develop.page:27
msgid ""
"If you would like to <link href=\"https://wiki.gnome.org/Apps/Games/"
"Contribute\">help develop <app>GNOME Games</app></link>, you can get in "
"touch with the developers at <link href=\"https://matrix.to/#/#gnome-games:"
"gnome.org\">#gnome-games:gnome.org</link> on <link xref=\"help:gnome-help/"
"help-matrix\">Matrix</link> or <link href=\"https://web.libera.chat/#gnome-"
"games\">#gnome-games</link> on irc.libera.chat, or via <link href=\"https://"
"discourse.gnome.org/tag/games\">GNOME Discourse</link>."
msgstr ""
"Jeśli chcesz <link href=\"https://wiki.gnome.org/Apps/Games/"
"Contribute\">pomóc rozwijać <app>Gry GNOME</app></link>, to możesz "
"skontaktować się z programistami na kanale <link href=\"https://matrix.to/#/"
"#gnome-games:gnome.org\">#gnome-games:gnome.org</link> w sieci <link "
"xref=\"help:gnome-help/help-matrix\">Matrix</link> lub <link href=\"https://"
"web.libera.chat/#gnome-games\">#gnome-games</link> w sieci irc.libera.chat, "
"albo przez serwis <link href=\"https://discourse.gnome.org/tag/games\">GNOME "
"Discourse</link> (w języku angielskim)."

#. (itstool) path: info/desc
#: C/documentation.page:21
msgid "Contribute to the documentation."
msgstr "Chcesz rozbudować dokumentację?"

#. (itstool) path: page/title
#: C/documentation.page:24
msgid "Help write documentation"
msgstr "Pomoc w pisaniu dokumentacji"

#. (itstool) path: page/p
#: C/documentation.page:25
msgid ""
"To contribute to the Documentation Project, feel free to get in touch with "
"us in <link href=\"https://matrix.to/#/#docs:gnome.org\">#docs:gnome.org</"
"link> on <link xref=\"help:gnome-help/help-matrix\">Matrix</link> or <link "
"href=\"https://web.libera.chat/#gnome-docs\">#gnome-docs</link> on irc."
"libera.chat, or via <link href=\"https://discourse.gnome.org/tag/"
"documentation\">GNOME Discourse</link>."
msgstr ""
"Jeśli chcesz wziąć udział w projekcie dokumentacji, to skontaktuj się z nami "
"na kanale <link href=\"https://matrix.to/#/#docs:gnome.org\">#docs:gnome."
"org</link> w sieci <link xref=\"help:gnome-help/help-matrix\">Matrix</link> "
"lub <link href=\"https://web.libera.chat/#gnome-docs\">#gnome-docs</link> "
"w sieci irc.libera.chat, albo przez serwis <link href=\"https://discourse."
"gnome.org/tag/documentation\">GNOME Discourse</link> (w języku angielskim)."

#. (itstool) path: page/p
#: C/documentation.page:32
msgid ""
"Our <link href=\"https://wiki.gnome.org/DocumentationProject/"
"Contributing\">wiki</link> page contains useful information."
msgstr ""
"Nasza <link href=\"https://wiki.gnome.org/DocumentationProject/"
"Contributing\">wiki</link> zawiera przydatne informacje (w języku "
"angielskim)."

#. (itstool) path: info/title
#: C/index.page:6
msgctxt "link:trail"
msgid "Quadrapassel"
msgstr "Quadrapassel"

#. (itstool) path: info/title
#: C/index.page:7
msgctxt "link"
msgid "Quadrapassel"
msgstr "Quadrapassel"

#. (itstool) path: info/title
#: C/index.page:8
msgctxt "text"
msgid "Quadrapassel"
msgstr "Quadrapassel"

#. (itstool) path: page/title
#: C/index.page:20
msgid "<_:media-1/> Quadrapassel"
msgstr "<_:media-1/> Quadrapassel"

#. (itstool) path: page/p
#: C/index.page:22
msgid ""
"In Quadrapassel, the GNOME version of Tetris, your goal is to create as many "
"complete horizontal lines as possible. The lines are made from seven "
"different shapes. The shapes fall randomly from the top center of the "
"screen. Your task is to rotate and move the shapes across the screen to make "
"complete lines. By making complete horizontal lines, you score points and "
"move up levels. As you progress, the speed of the falling blocks increases."
msgstr ""
"Celem gry Quadrapassel, wersji gry Tetris dla środowiska GNOME, jest "
"utworzenie możliwie jak największej liczby zapełnionych poziomych rzędów. "
"Rzędy są układane z siedmiu różnych kształtów. Spadają one losowo z góry "
"ekranu. Zadaniem gracza jest obrócenie i przesunięcie kształtów tak, aby "
"utworzyły wypełnione rzędy, co zapewnia punkty i kolejne poziomy. Im dalej, "
"tym klocki spadają szybciej."

#. (itstool) path: section/title
#: C/index.page:25
msgid "Game Play"
msgstr "Rozgrywka"

#. (itstool) path: section/title
#: C/index.page:28
msgid "Get Involved"
msgstr "Dołącz do nas"

#. (itstool) path: p/link
#: C/legal.xml:3
msgid "Creative Commons Attribution-ShareAlike 3.0 Unported License"
msgstr "Creative Commons Attribution-ShareAlike 3.0 Unported"

#. (itstool) path: license/p
#: C/legal.xml:3
msgid "This work is licensed under a <_:link-1/>."
msgstr "Na warunkach licencji <_:link-1/>."

#. (itstool) path: info/desc
#: C/license.page:8
msgid "Legal information."
msgstr "Informacje prawne."

#. (itstool) path: page/title
#: C/license.page:11
msgid "License"
msgstr "Licencja"

#. (itstool) path: page/p
#: C/license.page:12
msgid ""
"This work is distributed under a CreativeCommons Attribution-Share Alike 3.0 "
"Unported license."
msgstr ""
"Na warunkach licencji Creative Commons Attribution-Share Alike 3.0 Unported."

#. (itstool) path: page/p
#: C/license.page:20
msgid "You are free:"
msgstr "Można:"

#. (itstool) path: item/title
#: C/license.page:25
msgid "<em>To share</em>"
msgstr "<em>dzielić się</em>"

#. (itstool) path: item/p
#: C/license.page:26
msgid "To copy, distribute and transmit the work."
msgstr ", czyli kopiować, rozpowszechniać, odtwarzać i wykonywać utwór."

#. (itstool) path: item/title
#: C/license.page:29
msgid "<em>To remix</em>"
msgstr "<em>remiksować</em>"

#. (itstool) path: item/p
#: C/license.page:30
msgid "To adapt the work."
msgstr ", czyli tworzyć utwory zależne."

#. (itstool) path: page/p
#: C/license.page:33
msgid "Under the following conditions:"
msgstr "Na następujących warunkach:"

#. (itstool) path: item/title
#: C/license.page:38
msgid "<em>Attribution</em>"
msgstr "<em>Uznanie autorstwa</em>"

#. (itstool) path: item/p
#: C/license.page:39
msgid ""
"You must attribute the work in the manner specified by the author or "
"licensor (but not in any way that suggests that they endorse you or your use "
"of the work)."
msgstr ""
"Utwór należy oznaczyć w sposób określony przez Twórcę lub Licencjodawcę."

#. (itstool) path: item/title
#: C/license.page:46
msgid "<em>Share Alike</em>"
msgstr "<em>Na tych samych warunkach</em>"

#. (itstool) path: item/p
#: C/license.page:47
msgid ""
"If you alter, transform, or build upon this work, you may distribute the "
"resulting work only under the same, similar or a compatible license."
msgstr ""
"Zmieniając utwór, przekształcając go lub tworząc na jego podstawie, wolno "
"rozpowszechniać powstały utwór zależny jedynie na tej samej, podobnej lub "
"zgodnej licencji."

#. (itstool) path: page/p
#: C/license.page:53
msgid ""
"For the full text of the license, see the <link href=\"http://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons website</"
"link>, or read the full <link href=\"http://creativecommons.org/licenses/by-"
"sa/3.0/\">Commons Deed</link>."
msgstr ""
"Pełny tekst licencji dostępny jest na stronie <link href=\"http://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">Creative Commons</link>; "
"można również przeczytać <link href=\"http://creativecommons.org/licenses/by-"
"sa/3.0/\">podsumowanie</link>."

#. (itstool) path: license/p
#: C/playgame.page:19
msgid "Creative Commons Attribution Share Alike 3.0"
msgstr "Creative Commons Attribution Share Alike 3.0"

#. (itstool) path: info/desc
#: C/playgame.page:21
msgid "Rules and keyboard navigation."
msgstr "Zasady i używanie klawiatury."

#. (itstool) path: page/title
#. (itstool) path: section/title
#: C/playgame.page:24 C/playgame.page:27
msgid "How To Play"
msgstr "Jak grać"

#. (itstool) path: item/p
#: C/playgame.page:30
msgid ""
"The <key>←</key> and <key>→</key> keys move the blocks across the screen."
msgstr "Klawisze <key>←</key> i <key>→</key> przesuwają klocki po ekranie."

#. (itstool) path: item/p
#: C/playgame.page:31
msgid ""
"The <key>↑</key> key rotates the block. The rotation (which is "
"counterclockwise by default) can be changed in the <link "
"xref=\"preferences\">Preferences</link>."
msgstr ""
"Klawisz <key>↑</key> obraca klocek. Kierunek obrotu (domyślnie w lewo) można "
"zmienić w <link xref=\"preferences\">Preferencjach</link>."

#. (itstool) path: item/p
#: C/playgame.page:32
msgid ""
"The <key>SPACEBAR</key> and <key>↓</key> key increase the speed of the drop. "
"The <key>SPACEBAR</key> causes a hard-drop (instant move to the bottom). The "
"<key>↓</key> key causes a soft-drop (temporary increase in speed while the "
"key is held down)."
msgstr ""
"<key>Spacja</key> i klawisz <key>↓</key> zwiększają prędkość spadania. "
"<key>Spacja</key> powoduje natychmiastowe przeniesienie klocka na dół, "
"a <key>↓</key> tymczasowe zwiększenie prędkości, kiedy klawisz jest "
"przytrzymany."

#. (itstool) path: item/p
#: C/playgame.page:33
msgid ""
"The <key>Enter</key> will restart the game quickly once it is game over."
msgstr ""
"Klawisz <key>Enter</key> szybko rozpocznie grę od początku po jej "
"zakończeniu."

#. (itstool) path: section/title
#: C/playgame.page:38
msgid "Rules"
msgstr "Zasady"

#. (itstool) path: section/p
#: C/playgame.page:40
msgid "There are a couple of rules that you should keep in mind:"
msgstr "Należy pamiętać o dwóch zasadach:"

#. (itstool) path: item/p
#: C/playgame.page:42
msgid ""
"You can pause, end, start a new game, get help or quit the program at any "
"time."
msgstr ""
"W każdej chwili można wstrzymać, zakończyć, rozpocząć nową grę, uzyskać "
"pomoc lub wyłączyć program."

#. (itstool) path: item/p
#: C/playgame.page:43
msgid "You can pause the game as many times as you want."
msgstr "Można wstrzymywać grę dowolną liczbę razy."

#. (itstool) path: info/desc
#: C/preferences.page:21
msgid "Preferences for the game and the theme."
msgstr "Preferencje gry i motywu."

#. (itstool) path: page/title
#: C/preferences.page:25
msgid "Game Preferences"
msgstr "Preferencje gry"

#. (itstool) path: page/p
#: C/preferences.page:27
msgid ""
"To change the game preferences, press the menu button in the top-right "
"corner of the window and select <gui style=\"menuitem\">Preferences</gui>."
msgstr ""
"Aby zmienić preferencje gry, kliknij przycisk menu w górnym prawym rogu okna "
"i wybierz <gui style=\"menuitem\">Preferencje</gui>."

#. (itstool) path: page/p
#: C/preferences.page:29
msgid ""
"The three tabs available are <gui>Game</gui>, <gui>Controls</gui> and "
"<gui>Theme</gui>."
msgstr ""
"Trzy dostępne karty to <gui>Gra</gui>, <gui>Sterowanie</gui> i <gui>Motyw</"
"gui>."

#. (itstool) path: section/title
#: C/preferences.page:32
msgid "Game"
msgstr "Gra"

#. (itstool) path: section/p
#: C/preferences.page:33
msgid ""
"Under the <gui>Game</gui> tab, you can change the way in which the game "
"starts:"
msgstr "W karcie <gui>Gra</gui> można zmienić sposób, w jaki gra się zaczyna:"

#. (itstool) path: item/p
#: C/preferences.page:35
msgid ""
"<gui>Number of pre-filled rows</gui> indicates how many rows with blocks "
"there will be when the game starts."
msgstr ""
"<gui>Liczba początkowo wypełnionych rzędów</gui> wskazuje, ile rzędów ma być "
"wypełnionych klockami po rozpoczęciu gry."

#. (itstool) path: item/p
#: C/preferences.page:36
msgid ""
"<gui>Density of blocks in a pre-filled row</gui> indicates how many blocks "
"there will be in a pre-filled row when the game starts."
msgstr ""
"<gui>Gęstość klocków w początkowo wypełnionym rzędzie</gui> wskazuje, ule "
"klocków ma być w początkowo wypełnionym rzędzie po rozpoczęciu gry."

#. (itstool) path: item/p
#: C/preferences.page:37
msgid "<gui>Starting level</gui> controls the speed with which you will start."
msgstr ""
"<gui>Poziom początkowy</gui> kontroluje prędkość, z jaką gracz zaczyna."

#. (itstool) path: section/p
#: C/preferences.page:39
msgid "These options can vary from 0 to 15, 0 to 10, and 1 to 20 respectively."
msgstr "Te opcje mogą wynosić kolejno od 0 do 15, 0 do 10 i 1 do 20."

#. (itstool) path: section/p
#: C/preferences.page:41
msgid "Other available options are:"
msgstr "Pozostałe dostępne opcje:"

#. (itstool) path: item/p
#: C/preferences.page:43
msgid "<gui>Enable sounds</gui>"
msgstr "<gui>Dźwięki</gui>"

#. (itstool) path: item/p
#: C/preferences.page:44
msgid "<gui>Choose difficult blocks</gui>"
msgstr "<gui>Wybór trudnych klocków</gui>"

#. (itstool) path: item/p
#: C/preferences.page:45
msgid "<gui>Preview next block</gui>"
msgstr "<gui>Podgląd następnego klocka</gui>"

#. (itstool) path: item/p
#: C/preferences.page:46
msgid "<gui>Rotate blocks counterclockwise</gui>"
msgstr "<gui>Obrót klocków w lewo</gui>"

#. (itstool) path: item/p
#: C/preferences.page:47
msgid "<gui>Show where the block will land</gui>"
msgstr "<gui>Wyświetlanie miejsca lądowania klocka</gui>"

#. (itstool) path: section/title
#: C/preferences.page:52
msgid "Controls"
msgstr "Sterowanie"

#. (itstool) path: section/p
#: C/preferences.page:53
msgid ""
"By double-clicking on the given controls, thus selecting the new one, you "
"can change the control keys."
msgstr ""
"Można zmienić każdy klawisz sterowania podwójnie klikając go i wybierając "
"nowy."

#. (itstool) path: section/title
#: C/preferences.page:57
msgid "Theme"
msgstr "Motyw"

#. (itstool) path: section/p
#: C/preferences.page:58
msgid "To change the theme, choose a different one from the drop-down menu."
msgstr "Aby zmienić motyw, wybierz inny z rozwijanego menu."

#. (itstool) path: section/p
#: C/preferences.page:59
msgid "Four different themes are available for the blocks' contours:"
msgstr "Dostępne są cztery różne motywy klocków:"

#. (itstool) path: item/p
#: C/preferences.page:63
msgid "Plain"
msgstr "Zwykły"

#. (itstool) path: item/p
#: C/preferences.page:64
msgid "Tango Flat"
msgstr "Tango płaski"

#. (itstool) path: item/p
#: C/preferences.page:65
msgid "Tango Shaded"
msgstr "Tango cieniowany"

#. (itstool) path: item/p
#: C/preferences.page:66
msgid "Clean"
msgstr "Prosty"

#. (itstool) path: info/desc
#: C/score.page:21
msgid "High Scores."
msgstr "Najlepsze wyniki."

#. (itstool) path: page/title
#: C/score.page:24
msgid "Score"
msgstr "Wyniki"

#. (itstool) path: section/title
#: C/score.page:27
msgid "Score More"
msgstr "Jak zdobyć więcej punktów"

#. (itstool) path: section/p
#: C/score.page:28
msgid ""
"If you start from level 1, the only way to go to the next level is by "
"completing more lines, that is, scoring more points. In order for you to "
"have a better score, try to adapt the screen, the keyboard, and the level "
"(the speed) that fits you the most!"
msgstr ""
"Jeśli rozpoczęto od 1. poziomu, to jedynym sposobem na przejście do "
"następnego jest wypełnienie większej liczby wierszy, tj. zdobycie większej "
"liczby punktów. Aby zdobyć więcej punktów, spróbuj dopasować ekran, "
"klawiaturę i poziom (prędkość) do własnych upodobań!"

#. (itstool) path: section/p
#: C/score.page:29
msgid ""
"You can make many changes to the keyboard controls, the contour of the "
"blocks, the number and density of lines with which you start, and the speed "
"of drop. You can change your settings in the <link "
"xref=\"preferences\">Preferences</link>."
msgstr ""
"Można zmienić klawisze sterowania, wygląd klocków, liczbę i gęstość "
"początkowych rzędów oraz prędkość spadania w <link "
"xref=\"preferences\">Preferencjach</link>."

#. (itstool) path: section/title
#: C/score.page:33
msgid "High Scores"
msgstr "Najlepsze wyniki"

#. (itstool) path: section/p
#: C/score.page:34
msgid ""
"Press the menu button in the top-right corner of the window and select <gui "
"style=\"menuitem\">Scores</gui> to find the records from the highest to the "
"lowest score."
msgstr ""
"Kliknij przycisk menu w górnym prawym rogu okna i wybierz <gui "
"style=\"menuitem\">Wyniki</gui>, aby dowiedzieć się, kto osiągnął najlepszy "
"wynik."

#. (itstool) path: info/desc
#: C/translate.page:18
msgid "Do you know a world language?"
msgstr "Znasz jakiś język poza angielskim?"

#. (itstool) path: page/title
#: C/translate.page:21
msgid "Help translate"
msgstr "Pomoc w tłumaczeniu"

#. (itstool) path: page/p
#: C/translate.page:23
msgid ""
"The <app>GNOME games</app> user interface and documentation is being "
"translated by a world-wide volunteer community."
msgstr ""
"Interfejs i dokumentacja <app>Gier GNOME</app> jest tłumaczona przez "
"ogólnoświatową społeczność ochotników."

#. (itstool) path: page/p
#: C/translate.page:27
msgid ""
"There are <link href=\"https://l10n.gnome.org/module/quadrapassel/\">many "
"languages</link> for which translations are still needed."
msgstr ""
"<link href=\"https://l10n.gnome.org/module/quadrapassel/\">Wiele języków</"
"link> nadal potrzebuje tłumaczeń."

#. (itstool) path: page/p
#: C/translate.page:31
msgid ""
"To start translating you will need to <link href=\"https://l10n.gnome."
"org\">create an account</link> and join the <link href=\"https://l10n.gnome."
"org/teams/\">translation team</link> for your language. This will give you "
"the ability to upload new translations."
msgstr ""
"Do rozpoczęcia tłumaczenia potrzebne jest <link href=\"https://l10n.gnome."
"org\">utworzenie konta</link> i dołączenie do <link href=\"https://l10n."
"gnome.org/teams/\">zespołu tłumaczy</link> dla danego języka. Umożliwi to "
"wysyłanie nowych tłumaczeń."

#. (itstool) path: page/p
#: C/translate.page:35
msgid ""
"You can chat with GNOME translators in <link href=\"https://matrix.to/#/"
"#i18n:gnome.org\">#i18n:gnome.org</link> on <link xref=\"help:gnome-help/"
"help-matrix\">Matrix</link> or <link href=\"https://web.libera.chat/#gnome-"
"i18n\">#gnome-i18n</link> on irc.libera.chat. People on the channel are "
"located worldwide, so you may not get an immediate response as a result of "
"timezone differences."
msgstr ""
"Można skontaktować się z tłumaczami GNOME na kanale <link href=\"https://"
"matrix.to/#/#i18n:gnome.org\">#i18n:gnome.org</link> w sieci <link "
"xref=\"help:gnome-help/help-matrix\">Matrix</link> lub <link href=\"https://"
"web.libera.chat/#gnome-i18n\">#gnome-i18n</link> w sieci irc.libera.chat "
"(w języku angielskim). Z powodu różnic stref czasowych odpowiedź może nie "
"przyjść od razu."

#. (itstool) path: page/p
#: C/translate.page:43
msgid ""
"Alternatively, you can contact the Internationalization Team using <link "
"href=\"https://discourse.gnome.org/tag/i18n\">GNOME Discourse</link>."
msgstr ""
"Można także skontaktować się z zespołem tłumaczeń za pomocą serwisu <link "
"href=\"https://discourse.gnome.org/tag/i18n\">GNOME Discourse</link> "
"(w języku angielskim)."
